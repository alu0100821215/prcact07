require 'spec_helper'

describe Bibliogem do

	describe Nodo do
		refPrueba = Referencia.new(["Alan"],"El pedrolo",nil,"Indie", 234342, "01-01-2001", [123213],[12321312])
		nodoPrueba = Nodo.new(refPrueba, nil)
		it "Los nodos se crean correctamente" do
			nodoPrueba.should be_an_instance_of Nodo
		end
		it "Se ha introducido correctamente la referencia" do
			nodoPrueba[:valor].should be_an_instance_of Referencia
		end
	end
	
	describe Lista do
		refPrueba = Referencia.new(["Alan"],"El pedrolo",nil,"Indie", 234342, "01-01-2001", [123213],[12321312])
		nodoPrueba = Nodo.new(refPrueba.clone,nil)
		listaPrueba = Lista.new()
		listaPrueba.push_inicio(nodoPrueba.clone)
		it "Se puede insertar un elemento" do
			expect(listaPrueba.n_elementos).to be > 0
		end
		it "Se extrae el primer elemento de la lista" do
			listaPrueba.pop_inicio.should be_an_instance_of Nodo
		end
		listaPrueba2 = Lista.new()
		listaPrueba2.push_inicio(nodoPrueba.clone)
		listaPrueba2.push_inicio(nodoPrueba.clone)
		it "Se pueden insertar varios elementos" do
			expect(listaPrueba2.n_elementos).to be >= 2
		end
		it "Debe existir una lista con su cabeza" do
			listaPrueba.nodo_inicial.should be_an_instance_of Nodo
		end
	end
	# autores, titulo, serie, editorial, nedicion, fecha, isbn10, isbn13
end
