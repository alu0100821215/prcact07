class Referencia
	attr_reader :autores, :titulo, :serie, :editorial, :nedicion, :fecha, :isbn10, :isbn13
	def initialize(aut,tit,ser,edi,ne,fe,is10,is13)
		if aut.any?
			@autores=aut
		else
			raise ArgumentError.new("Error: Se requiere al menos un autor")
		end
		if !tit.nil?
			@titulo=tit
		else
			raise ArgumentError.new("Error: Se requiere un tiulo")
		end
		@serie=ser
		if !edi.nil?
			@editorial=edi
		else
			raise ArgumentError.new("Error: Se requiere una editorial")
		end
		if !ne.nil?
			@nedicion=ne
		else
			raise ArgumentError.new("Error Se requiere un numero de edicion")
		end
		if !fe.nil?
			@fecha=Date::strptime(fe, "%d-%m-%Y")
		else
			raise ArgumentError.new("Error: Se requiere una fecha con formato %d-%m-%Y")
		end
		if !is10.nil? || !is13.nil?
			@isbn10=is10
			@isbn13=is13
		else
			raise ArgumentError.new("Error: Se requiere al menos un codigo isbn")
		end
	end

	def to_s
		"#{@autores}, #{@titulo}, #{@series}, #{@editorial}, #{@nedicion}, #{@fecha}, #{@isbn10}, #{@isbn13}"
	end
end
