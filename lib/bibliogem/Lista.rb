require "/home/victor/Escritorio/LPP/pract06/lib/bibliogem/Referencia"

Nodo = Struct.new(:valor, :siguiente) do
end

class Lista
	attr_reader :n_elementos, :nodo_inicial, :nodo_final
	def initialize
		@n_elementos = 0
		@nodo_inicial = Nodo.new()
		@nodo_final = @nodo_inicial
	end
	def push_inicio(valor)
		if @n_elementos == 0
			@nodo_inicial[:valor]=valor
			@nodo_final = @nodo_inicial
			@n_elementos+=1
		elsif @n_elementos>=1
			aux = Nodo.new(valor, @nodo_inicial)
			@nodo_inicial = aux
			@n_elementos+=1
		end
	end
	def push_final(valor)
		if(@n_elementos == 0)
			@nodo_inicial[:valor]=valor
			@nodo_final = @nodo_inicial
			@n_elementos+=1
		end
		if(@n_elementos >= 1)
			aux = Nodo.new(valor, nil)
			@nodo_final.set_siguiente(aux)
			@nodo_final = aux
			@n_elementos+=1
		end
	end
	def insert(pos, valor)
		if(pos < @n_elementos)
			aux = @nodo_inicial
			for i in 0..pos
				aux=aux[:siguiente]
			end
			insert = Nodo.new(valor, aux[:siguiente])
			aux[:siguiente]=insert
			@n_elementos+=1
		else
			puts "No hay tantos elementos."
		end
	end
	def delete(pos)
		if(pos < @n_elementos)
			aux = @nodo_inicial
			for i in 0..(pos-1)
				aux=aux[:siguiente]
			end
			delete = aux[:siguiente]
			aux.set_siguiente(delete[:siguiente])
			delete.set_siguiente(nil)
			delete.set_valor(nil)
			@n_elementos-=1
		else
			puts "No hay tantos elementos."
			return nil
		end
	end
	def pop_inicio
		if(@n_elementos == 0)
			puts "La lista esta vacia"
			return nil
		elsif(@n_elementos == 1)
			ret = Nodo.new(@nodo_inicial[:valor], nil)
			@nodo_inicial[:valor]=nil
			@nodo_final = @nodo_inicial
			@n_elementos-=1
			return ret
		elsif(@n_elementos > 1)
			aux = @nodo_incial.clone
			ret = Nodo.new(@nodo_inicial[:valor], nil)
			@nodo_inicial=@nodo_inicial[:siguiente]
			aux[:siguiente]=nil
			aux[:valor]=nil
			@n_elementos-=1
			return ret
		end
	end
	def pop_final
		if(@n_elementos == 0)
			puts "La lista esta vacia"
			return nil
		elsif(@n_elementos == 1)
			ret = Nodo.new(@nodo_inicial[:valor], nil)
			@nodo_incial[:valor]=nil
			@nodo_final = @nodo_inicial
			@n_elementos-=1
			return ret
		elsif(@n_elementos > 1)
			aux = @nodo_inicial.clone
			ret = Nodo.new(@nodo_inicial[:valor], nil)
			for i in 0..(@n_elementos-2)
				aux=aux[:siguiente]
			end
			@nodo_final[:valor]=nil
			@nodo_final=aux
			@n_elementos-=1
			return ret
		end
	end
end
